@extends('layout.layout')
@section('content')                

    <link href="{{URL::to('/vendor/chart.js/Chart.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{URL::to('/vendor/chart.js/Chart.min.js')}}"></script>



    <link href="{{URL::to('/css/maps.css')}}" rel="stylesheet" type="text/css">
    <style>
        .col{
            margin-bottom: 3px;
            border-collapse: collapse;
        }
        .linha_bot{
            border-bottom: 1px solid black;
        }
        .row{
            border-collapse: collapse;
        }
        canvas{
            
        }
        
    </style>

    <div id="escola" class="row align-middle text-center">
        <div class="col linha_bot">
            <span class="campus">Geral</span>
        </div>
        <div class="col linha_bot" style="border-left: 1px solid black;">
            <span class="campus">UA</span>
        </div>
        <div class="col linha_bot" style="border-left: 1px solid black;border-right: 1px solid black;">        
            <span class="campus">ESAN</span>
        </div>            
        <div class="col linha_bot">
            <span class="campus">ESTGA</span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12 col-lg-8 offset-lg-2">
            <div class="card shadow mb-4">
                <div class="card-header py-3 text-center">
                    <h6 class="m-0 font-weight-bold text-primary">Ocorrencias por Estado</h6>
                </div>
                <div class="card-body">
                    <canvas id="OcorrenciasEstado"></canvas>
                </div>
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8 offset-lg-2">
            <div class="card shadow mb-4">
                <div class="card-header py-3 text-center">
                    <h6 class="m-0 font-weight-bold text-primary">Ocoorencias por Tipo</h6>
                </div>
                <div class="card-body">
                    <canvas id="OcorrenciasTipo"></canvas>
                </div>
            </div>
        </div>
    </div>




    
    <script type="text/javascript" src="{{ URL::to('/js/graficos.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var campus = "Geral"
            var URL = "{{ URL::to('/')}}"
            piechart(URL + "/OcorrenciasEstado/" + campus,"OcorrenciasEstado");
            barchart(URL + "/OcorrenciasTipo/" + campus,"OcorrenciasTipo");
            
            $(".campus").click(function(){
                campus = $(this).html();
                piechart(URL + "/OcorrenciasEstado/" + campus,"OcorrenciasEstado");
                barchart(URL + "/OcorrenciasTipo/" + campus,"OcorrenciasTipo");
            })

        });
    </script>

    
@stop