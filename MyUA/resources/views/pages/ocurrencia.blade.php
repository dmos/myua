@extends('layout.layout')
@section('content')                

<link href="{{URL::to('/css/maps.css')}}" rel="stylesheet" type="text/css">
<style>
    .map{
        height: 50vh;
    }
</style>


<div class="row">
    <!-- div info -->
    <div class="col-12 col-lg-6">

        <div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
                <h6 class="m-0 font-weight-bold text-primary">Informações</h6>
            </div>
            <div class="card-body">

                @foreach($ocorrencia as $key => $value)
                <p>
                    <b>{{$key}} : </b> <span id="{{$key}}">{{$value}}</span>
                </p>
                @endforeach
            </div>
        </div>
    </div>


    <!-- div mapa + imag-->
    <div class="col-12 col-lg-6">

        <div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
                <h6 class="m-0 font-weight-bold text-primary">Mapa</h6>
            </div>
            <div class="card-body">
                <div id="map" class="map"></div>
            </div>
        </div>

        @if (file_exists(public_path('img/ocorrencia/' .$ocorrencia->id. '.jpeg'))) 
        <div class="card shadow mb-4 text-center">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Imagem</h6>
            </div>
            <div class="card-body">
                <img src="{{url('img/ocorrencia/' .$ocorrencia->id. '.jpeg')}}">
            </div>
        </div>
        @endif
        
    </div>
    


    <script>

        //extent: [minx,miny,maxx,maxy],
                //map.getView().calculateExtent(map.getSize())
                //ol.proj.transformExtent([-940162.2853725493, 4949642.9262222955, -939799.2094881948, 4949844.767750111],'EPSG:900913','EPSG:4326')

        if($("#localizacao").html() == "UA"){
            long    =   -8.65535;
            lat     =   40.63062;
            minzoom =   17;
            var maxExtent =[-8.66293,40.63707,
                            -8.65323,40.62124]
        }
        if($("#localizacao").html() == "ESAN"){
            long =   -8.47721;
            lat  =   40.86218;
            minzoom =   18;
            var maxExtent =[-8.47836,40.86252,
                            -8.47573,40.86196]
        }
        if($("#localizacao").html() == "ESTGA"){
            long =   -8.44378;
            lat  =   40.57394;
            minzoom =   18;
            var maxExtent =[-8.44529,40.57482,
                            -8.44314,40.57310]
                            
        }
    
        if($("#localizacao").html() == "ISCA"){
            long    =   -8.65297;
            lat     =   40.63025;
            minzoom =   18; 
            var maxExtent =[-8.65437,40.63149,
                            -8.65051,40.62901]
        }

        var map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({ source: new ol.source.OSM() }),
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([long,lat]),
                zoom: 18,
                minZoom: minzoom,
                maxZoom: 20,
                extent: ol.proj.transformExtent(maxExtent, 'EPSG:4326', 'EPSG:900913'),
                

                //extent: [-940162.2853725493, 4949642.9262222955, -939799.2094881948, 4949844.767750111]
            })
        });

        /*



        var marker = new ol.Feature({
            geometry: new ol.geom.Point(
                ol.proj.fromLonLat([-8.65535,40.63064])
            ),  
        });
        marker.setStyle(new ol.style.Style({
            image: new ol.style.Icon(({
                crossOrigin: 'anonymous',
                src: "{{ URL::asset('/img/marker.svg') }}",
                scale: .05,
            }))
        }));


        var vectorSource = new ol.source.Vector({
            features: [marker]
        });

        var markerVectorLayer = new ol.layer.Vector({
            source: vectorSource,
        });
        map.addLayer(markerVectorLayer);       

        map.getView().fit(vectorSource.getExtent());
        mapView.setZoom(18);

        */
        
        /*
        var features;
        $.ajax({
            type: "GET",
            url: "{{URL::to('/ocurrenciaGeoJSON')}}/"+$("#id").html(),
            dataType: 'json',
            async: true,
            success: function (response) {
                console.log(response)
                console.log(response.features[0])
                feat = response.features[0]
                features = new ol.format.GeoJSON().readFeatures(feat);
            }
        });

        */

    </script>

</div> <!-- end of row -->


<script>

    $.ajax({
        type: "GET",
        url: "{{URL::to('/ocurrenciaGeoJSON')}}/" + $("#id").html(),
        dataType: 'json',
        async: true,
        success: function (geojsonObject) {
            console.log(geojsonObject);
            
            var features = new ol.format.GeoJSON().readFeatures(geojsonObject, {
                featureProjection: 'EPSG:3857'
            });
            
            var vectorSource = new ol.source.Vector({ features: features });
            var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
                style: function (feature, resolution) {
                    console.log(feature.getProperties(), feature.get('marker-color'));
                    return [new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            fill: new ol.style.Fill({ color: feature.get('marker-color') })
                        })
                    })];
                }
            });


            map.addLayer(vectorLayer);
            map.getView().fit(vectorSource.getExtent(), map.getSize());

        }
    });




</script>

@stop