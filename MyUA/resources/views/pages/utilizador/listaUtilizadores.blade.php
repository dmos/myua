@extends('layout.layout')
@section('content')                

    <link rel="stylesheet" type="text/css" href="{{URL::to("/vendor/datatables/dataTables.bootstrap4.css")}}">

    <script type="text/javascript" src="{{URL::to("/vendor/datatables/jquery.dataTables.js")}}"></script>
    <script type="text/javascript" src="{{URL::to("/vendor/datatables/dataTables.bootstrap4.js")}}"></script>
    <script type="text/javascript" src="{{URL::to("/js/datatable_custom.js")}}"></script>


    <table id="mytable"  class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nº</th>
                <th>Nome</th>
                <th>Tipo de utilizador</th>
                <th>Email </th>
                <th>Ações</th>
            </tr>
        </thead>

    </table>

    <script>
        var table = createDataTable("#mytable","listaUtilizadoresJSON")
        $('#mytable tbody').on( 'click', 'button', function () {
            var data = table.row( $(this).parents('tr') ).data();
            window.location= "utilizador/"+data[0]
        });
    </script>

@stop