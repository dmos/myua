<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MyUA - {{Route::current()->getName()}}</title>

    <!-- link rel="stylesheet" href="{{URL::to('/vendor/my.css')}}" -->

    <link href="{{URL::to('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{URL::to('/css/sb-admin-2.css')}}" rel="stylesheet">

    
    
    <!-- JS SCRIPTS -->

    <!-- Bootstrap core JavaScript-->
    <script src="{{URL::to('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::to('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{URL::to('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>    
    

</head>


<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->

            <div class="row">
                <div class="col-lg-6 d-none d-lg-block text-center">
                    <h1 class="text-gray-900 mt-5">MyUA</h1>
                    <img src="{{url('img/logo_UA.png')}}" width="90%">
                </div>


                <div class="col-lg-6">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Bem-vindo!</h1>
                        </div>

                        <form class="user" method="post" action="{{url('/checkLogin')}}" id="loginForm">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" name="username" id="username" value="" placeholder="Introduza o nome de utilizador">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control form-control-user" name="password" id="password" value="" placeholder="Introduza a password">
                            </div>

                            <input type="submit" value="Login" class="btn btn-primary btn-user btn-block">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </div>

    </div>

  </div>


    <!-- JS SCRIPTS -->

    <!-- Custom scripts for all pages-->
    <script src="{{URL::to('/js/sb-admin-2.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{URL::to('/vendor/chart.js/Chart.min.js')}}"></script>

</body>

</html>
