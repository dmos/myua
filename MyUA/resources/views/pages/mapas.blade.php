@extends('layout.layout')
@section('content')                

    <!--
        OpenLayers Helpers
        
        https://wiki.openstreetmap.org/wiki/OpenLayers_Simple_Example 
    
        https://medium.com/attentive-ai/working-with-openlayers-4-part-2-using-markers-or-points-on-the-map-f8e9b5cae098

        https://embed.plnkr.co/plunk/hhEAWk
    -->


    <link href="{{URL::to('/css/maps.css')}}" rel="stylesheet" type="text/css">
    <style>
        .custom-control {
            font-size: 10pt;
            top: 80px;
            left: 10px;
            width: 100px;
            background-color: rgba(0,60,136,.5);
            color: white;
        }
        .custom-control:hover {
            background-color: rgba(0,60,136,.5);
            color: white;
        }
        .col{
            margin-bottom: 3px;
            border-collapse: collapse;
        }
        .linha_bot{
            border-bottom: 1px solid black;
        }
        .row{
            border-collapse: collapse;
        }
    </style>

    <div id="escola" class="row align-middle text-center">
        <div class="col-4 linha_bot">
            <span class="campus" data-lon="-8.65535" data-lat="40.63062" data-zoom="17">UA</span>
        </div>
        <div class="col-4 linha_bot" style="border-left: 1px solid black;border-right: 1px solid black;">        
            <span class="campus" data-lon="-8.47721" data-lat="40.86218" data-zoom="18">ESAN</span>
        </div>            
        <div class="col-4 linha_bot">
            <span class="campus" data-lon="-8.44378" data-lat="40.57394" data-zoom="18">ESTGA</span>
        </div>

        <div class="col-6">
            <span class="tipoMapa">Mapa de ocorrencias</span>
        </div>
        <div class="col-6" style="border-left: 1px solid black;">        
            <span class="tipoMapa">Mapa de calor</span>
        </div> 
    </div>

        
    <div id="map" class="map">
    </div>

    <div id="popup" class="ol-popup">
        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
        <div id="popup-content"></div>
    </div>


    <div class="row">
        <div class="col-sm-12">
          Legenda:
          <p>
            <a class="badge badge-success" href="#">Resolvidos</a>
            <a class="badge badge-warning" href="#">Em resolução</a>
            <a class="badge badge-danger" href="#">Aberto</a>
          </p>
        </div>
    </div>
    
    
    <script type="text/javascript">

        var vectorLayer;
        var features;
        var vectorSource;

        var filtro=new Array(); //array com os filtros que vão ser exluidos
        var filtroEstado = new Array();
        var tipoMapa = "Mapa de ocorrencias";
        var campusAtual = "UA";
        var maxExtent = [-8.66293,40.63707,
                        -8.65323,40.62124];
        

        // POP-UP
        var container = document.getElementById('popup');
        var content = document.getElementById('popup-content');
        var closer = document.getElementById('popup-closer');

        var overlay = new ol.Overlay({
            element: document.getElementById('popup'),
            autoPan: true,
            autoPanAnimation: {
            duration: 250
            }
        });

        closer.onclick = function() {
            overlay.setPosition(undefined);
            closer.blur();
            return false;
        };



        // control
        var cControl;
        customControl = function(opt_options) {
            cControl = document.createElement('div');
            cControl.id = "cControl"
            cControl.className = 'custom-control ol-unselectable ol-control';
            //cControl.innerHTML='<input type="checkbox" name="vehicle1" value="Bike"> I have a bike<br>'
            ol.control.Control.call(this, {
                element: cControl
            });
        };
        ol.inherits(customControl, ol.control.Control);



        var map = new ol.Map({
            overlays: [overlay],
            target: 'map',
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
                }).extend([
                new customControl()
            ]),
            layers: [
                new ol.layer.Tile({ source: new ol.source.OSM() }),
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([-8.65535,40.63062]),
                zoom: 16,
                minZoom: 17,
                maxZoom: 20,
                extent: ol.proj.transformExtent(maxExtent, 'EPSG:4326', 'EPSG:900913'),
                
            }),
        });


        getOcorrenciasCampus(campusAtual)
        

        $(".campus").click(function(){
            if( $(this).html() == campusAtual)
                return;

            campusAtual = $(this).html();

            if(campusAtual == "UA"){
                long    =   -8.65535;
                lat     =   40.63062;
                minzoom =   17;
                maxExtent =[-8.66293,40.63707,
                                -8.65323,40.62124]
            }
            if(campusAtual == "ESAN"){
                long =   -8.47721;
                lat  =   40.86218;
                minzoom =   18;
                maxExtent =[-8.47836,40.86252,
                                -8.47573,40.86196]
            }

            if(campusAtual == "ESTGA"){
                long =   -8.44378;
                lat  =   40.57394;
                minzoom =   18;
                maxExtent =[-8.44529,40.57482,
                                -8.44314,40.57310]                   
            }

            
            map.setView(new ol.View({
                center: ol.proj.fromLonLat([long, lat]),
                zoom: minzoom,
                minZoom: minzoom,
                maxZoom: 20,
                extent: ol.proj.transformExtent(maxExtent, 'EPSG:4326', 'EPSG:900913'),
                
            }))

            //console.log(Lon + " | " + Lat)     
            //mapView.setCenter([Lon,Lat])
            //map.getView().setCenter(ol.proj.transform([Lon, Lat], 'EPSG:4326', 'EPSG:3857'))
            //map.getView().setZoom(Zoom)

            map.removeLayer(vectorLayer); //remover lista de pontos
            filtro = [];
            cControl.innerHTML="";
            if (tipoMapa == "Mapa de ocorrencias"){
                getOcorrenciasCampus(campusAtual);
            }
            else{
                getHeatMapCampus(campusAtual);
            }
            
        });
         /*
        var marker = new ol.Feature({
            geometry: new ol.geom.Point(
                ol.proj.fromLonLat([-8.65535,40.63064])
            ),  
        });
        marker.setStyle(new ol.style.Style({
            image: new ol.style.Icon(({
                crossOrigin: 'anonymous',
                src: "{{ URL::asset('/img/marker.svg') }}",
                scale: .05,
            }))
        }));


        var vectorSource = new ol.source.Vector({
            features: [marker]
        });

        var markerVectorLayer = new ol.layer.Vector({
            source: vectorSource,
        });
        map.addLayer(markerVectorLayer);
        */


        function getOcorrenciasCampus(nomeCampus){
            var filtroExluir = JSON.stringify(filtro);
            var filtroExluirEstado = JSON.stringify(filtroEstado)
            var resize = true;
            $.ajax({
                type: "GET",
                url: "{{URL::to('/OcorrenciasCampus')}}/"+nomeCampus,
                dataType: 'json',
                data : {filtroExluir : filtroExluir, filtroExluirEstado : filtroExluirEstado},
                async: true,
                success: function (geojsonObject) {
                    map.removeLayer(vectorLayer);

                    console.log(geojsonObject);
                    if(cControl.innerHTML==""){
                        var tipos= new Set();
                        for (i =0; i < geojsonObject.features.length; i++) {
                            tipos.add(geojsonObject.features[i].properties.tipo)
                        }

                        tipos.forEach(function(tipo){  
                            cControl.innerHTML+='<p><input class="tipo" type="checkbox" name="'+tipo+'" value="'+tipo+'" checked>'+tipo+'</p>';  
                        })
                        cControl.innerHTML+='<hr style="border: 2px solid white;">';

                        //tipoEstado
                        var estados= new Set();
                        for (i =0; i < geojsonObject.features.length; i++) {
                            estados.add(geojsonObject.features[i].properties.tipoEstado)
                        }

                        estados.forEach(function(estado){  
                            cControl.innerHTML+='<p><input class="estado" type="checkbox" name="'+estado+'" value="'+estado+'" checked>'+estado+'</p>';  
                        })
                    }

                    features = new ol.format.GeoJSON().readFeatures(geojsonObject, {
                        featureProjection: 'EPSG:3857'
                    });
                    
                    vectorSource = new ol.source.Vector({ features: features });
                    vectorLayer = new ol.layer.Vector({
                    source: vectorSource,
                        style: function (feature, resolution) {
                            //console.log(feature.getProperties(), feature.get('marker-color'));
                            /*
                            return [new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 5,
                                    fill: new ol.style.Fill({ color: feature.get('marker-color') })
                                })
                            })];
                            */
                            return [new ol.style.Style({
                                image: new ol.style.Icon({
                                    color: feature.get('marker-color'),
                                    src: '{{URL::to("/img/marker.png")}}',
                                    crossOrigin: 'anonymous',
                                    anchor: [0.5, 0.5],
                                    size: [38,51],
                                    offset: [0, 0],
                                    opacity: 1,
                                    scale: 0.6,
                                })
                            })];
                        }
                    });

                    map.addLayer(vectorLayer);
                    if(resize){
                        console.log(resize);
                        map.getView().fit(vectorSource.getExtent(), map.getSize());
                    }
                }
            });
            
        }

        //ativar o popup com as informações da ocorrencia
        map.on("singleclick", function(e) {
            map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
                console.log(feature.getProperties())
                var f= feature.getProperties();
                var innerHTML;
                
                innerHTML = "<p><b> Nº da ocorrencia :</b> " + f.id  +"</p>";                 
                innerHTML += "<p><b>Tipo de ocorrencia :</b> " + f.tipo  +"</p>";
                innerHTML += "<p><b>Data da ocorrencia :</b> " + f.dataOcorre  +"</p>";
                innerHTML += "<p><a href='{{URL::to('/ocurrencia')}}/" + f.id + "' class='btn btn-primary'>Ver Ocorrencia</a></p>";
            
                content.innerHTML = innerHTML;
                overlay.setPosition(e.coordinate);
            })
        });
       
       //aplicar novo filtro
        $(document).on("change","input.tipo",function(){
            var filtroAplicar = $(this).prop("value");
            console.log(filtroAplicar);
            if($(this).prop( "checked" )){
                //console.log("checked")
                filtro = filtro.filter(e=>e !== filtroAplicar)
            }
            else{
                //console.log(" not checked")
                filtro.push(filtroAplicar);
            }
            console.log("alteração nos filtros: ", filtro)
            
            getOcorrenciasCampus(campusAtual);
            
        });

        $(document).on("change","input.estado",function(){
            var filtroAplicarEstado = $(this).prop("value");
            console.log(filtroAplicarEstado);
            if($(this).prop( "checked" )){
                //console.log("checked")
                filtroEstado = filtroEstado.filter(e=>e !== filtroAplicarEstado)
            }
            else{
                //console.log(" not checked")
                filtroEstado.push(filtroAplicarEstado);
            }
            console.log("alteração nos filtros: ", filtroEstado)
            getOcorrenciasCampus(campusAtual);
        });


        //auto refresh ocorrencias
        setInterval(getOcorrenciasCampus(campusAtual), 5000);


        function getHeatMapCampus(nomeCampus){
            $.ajax({
                type: "GET",
                url: "{{URL::to('/HeatMap')}}/"+nomeCampus,
                dataType: 'json',
                async: true,
                success: function (geojsonObject) {
                    console.log(geojsonObject);

                    features = new ol.format.GeoJSON().readFeatures(geojsonObject, {
                        featureProjection: 'EPSG:3857'
                    });
                    
                    vectorSource = new ol.source.Vector({ features: features });
                    vectorLayer = new ol.layer.Heatmap({
                        source: vectorSource,
                        radius: 12,
                        blur:15,
                    });

                    map.addLayer(vectorLayer);
                }
            });
        }

        $(".tipoMapa").click(function(){
            if( $(this).html() == tipoMapa)
                return;
            tipoMapa = $(this).html();
            console.log(tipoMapa);
            map.removeLayer(vectorLayer); //remover lista de pontos
            if(tipoMapa == "Mapa de ocorrencias"){
                $("#cControl").removeClass("d-none")
                getOcorrenciasCampus(campusAtual)
            }
            else{
                filtro = [];
                cControl.innerHTML="";
                $("#cControl").addClass("d-none")
                getHeatMapCampus(campusAtual);
            }
            

        });        

    </script>
@stop