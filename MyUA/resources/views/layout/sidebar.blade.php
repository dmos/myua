<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">MyUA</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if (Request::is('mapas')) {{'active'}} @endif">
        <a class="nav-link" href="{{URL::to('/mapas')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Lista de ocurrencias -->
    <li class="nav-item @if (Request::is('listaOcurrencias')) {{'active'}} @endif">
        <a class="nav-link" href="{{URL::to('listaOcurrencias')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Lista de ocorrencias</span>
        </a>
    </li>


    <!-- Estatisticas -->
    <li class="nav-item @if (Request::is('estatisticas')) {{'active'}} @endif">
        <a class="nav-link" href="{{URL::to('estatisticas')}}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Estatisticas</span>
        </a>
    </li>


    <!-- Utilizadores -->
    <li class="nav-item @if (Request::is('listaUtilizadores')) {{'active'}} @endif">
        <a class="nav-link" href="{{URL::to('listaUtilizadores')}}">
            <i class="fas fa-fw fa-user"></i>
            <span>Utilizadores</span>
        </a>
    </li>


    <!-- Preferencias -->
    <li class="nav-item @if (Request::is('preferencias')) {{'active'}} @endif">
        <a class="nav-link" href="{{URL::to('#')}}">
            <i class="fas fa-fw fa-cog"></i>
            <span>Preferencias</span>
        </a>
    </li>
    


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    
</ul>
    