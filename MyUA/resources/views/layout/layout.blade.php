<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MyUA - {{Route::current()->getName()}}</title>

    <!-- link rel="stylesheet" href="{{URL::to('/vendor/my.css')}}" -->

    <link href="{{URL::to('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{URL::to('/css/sb-admin-2.css')}}" rel="stylesheet">
    <!-- OpenLayers-->
    <link rel="stylesheet" href="{{URL::to('/vendor/OpenLayers/ol.css')}}">


    
    
    <!-- JS SCRIPTS -->

    <!-- Bootstrap core JavaScript-->
    <script src="{{URL::to('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::to('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{URL::to('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>    
    
    <!-- OpenLayers-->
    <script src="{{URL::to('/vendor/OpenLayers/ol.js')}}"></script>


</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layout.sidebar')
        <!-- End of Sidebar -->


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
    
            <!-- Main Content -->
            <div id="content">
                @include('layout.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    @yield('content')
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            
        </div>
        <!-- End of Content Wrapper -->
    
    </div>
    <!-- End of Page Wrapper -->




    <!-- JS SCRIPTS -->

    <!-- Custom scripts for all pages-->
    <script src="{{URL::to('/js/sb-admin-2.js')}}"></script>
    

    <!-- Page level plugins -->
    <script src="{{URL::to('/vendor/chart.js/Chart.min.js')}}"></script>

</body>
</html>

