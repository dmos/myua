var cores = ['rgba(255, 206, 86, 0.8)',
'rgba(20, 159, 64, 0.8)',
'rgba(255, 5, 64, 0.8)',
'rgba(75, 192, 192, 0.8)',
'rgba(153, 102, 255, 0.8)',
'rgba(255, 159, 64, 0.8)',
'rgba(100, 100, 12, 0.8)',
'rgba(255, 99, 132, 0.8)',
'rgba(54, 162, 235, 0.8)',
'rgba(5, 5, 5, 0.8)'];

var pie;
var mypie;

function piechart(route,elemente){
    $.getJSON( route, function( data ) {
      //console.log(data);
      //console.log("entrou");
      var fetched = data.dados;
      
  
      var labels=[];
      var dados=[];
      var cor= [];
  
      for(var i = 0; i<fetched.length; i++){
        labels.push(fetched[i]['label']);
        dados.push(fetched[i]['total']);
        cor.push(cores[i]);
      }
  
      //use to clear all variables and clear graphs.
      //if not done, can ocurre in visual bug on graph
      if(mypie != undefined || mypie != null){
        mypie.clear();
        mypie.destroy();
        pie = null;
        mypie = null;
  
      }
  
      pie = document.getElementById(elemente).getContext('2d');
  
      mypie = new Chart(pie, {
          type: 'pie',
          data: {
              labels: labels,
              datasets:[{
                  label: labels[i],
                  fill:false,
                  backgroundColor: cor,
                  borderColor: cor,
                  data: dados
              }]
            },
            options: {
              responsive: true,
              legend: {
                display: true,
                position :'bottom',
                }
            }
        });
  
        mypie.update();
    });
}  


var bar;
var mybar;

function barchart(route,elemente){
    $.getJSON( route, function( data ) {
      //console.log(data);
      //console.log("entrou");
      var fetched = data.dados;
      
  
      var labels=[];
      var dados=[];
      var cor= [];
  
      for(var i = 0; i<fetched.length; i++){
        labels.push(fetched[i]['label']);
        dados.push(fetched[i]['total']);
        cor.push(cores[i]);
      }
  
      //use to clear all variables and clear graphs.
      //if not done, can ocurre in visual bug on graph
      if(mybar != undefined || mybar != null){
        mybar.clear();
        mybar.destroy();
        bar = null;
        mybar = null;
  
      }
  
      bar = document.getElementById(elemente).getContext('2d');
  
      mybar = new Chart(bar, {
          type: 'bar',
          data: {
              labels: labels,
              datasets:[{
                label: labels[i],
                  fill:false,
                  backgroundColor: cor,
                  borderColor: cor,
                  data: dados,
              }]
            },
            options: {
              responsive: true,
              legend: {
                display: false,
                position :'bottom',
              },
              scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
              }
            
            }
        });
  
        mybar.update();
    });
}  