<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use DB;
use View;


class MapsController extends Controller
{
    
    public function index(){
        //$this->dbTeste();
        return view('pages.mapas');
    }


    public function OcorrenciasCampus($nomeCampus){
        //dd($_GET);
        $filtroExluir= json_decode($_GET['filtroExluir']);
        $filtroExluirEstado= json_decode($_GET['filtroExluirEstado']);
        //dd($filtroExluir);


        $geometry = DB::table('ocorrencia_osm as o')
        ->join('campus', 'o.campus_ID', '=', 'campus.id')
        ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
        ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
        ->join('estado as e','o.estado_ID','e.id')
        ->selectRaw('ST_AsGeoJSON(o.geom) as geometry, o.id ,o."estado_ID",o."dataOcorre",t."id" as idTipo,t."tipo" , e.tipo as "tipoEstado"')
        ->where('campus.localizacao',$nomeCampus)
        ->whereNotIn('t.tipo', $filtroExluir)
        ->whereNotIn('e.tipo', $filtroExluirEstado)
        ->get()
        ->toJSON();

        $original_data = json_decode($geometry, true);
        $features = array();

        foreach($original_data as $key => $value) {
            if($value['estado_ID'] == 1){
                $cor = "#cc522c"; //aberto
            }
            elseif ($value['estado_ID'] == 2) {
                $cor = "#efff7a"; //em resolução
            }
            else{
                $cor = "#42f459";//resolvido
            }
            //dd($value['geometry']);
            $features[] = array(
                'type' => 'Feature',
                'crs'       => array('type' => 'name','properties' => array( 'name' => 'EPSG:4326')),  
                'geometry' => json_decode($value['geometry']),
                'properties'    => array('marker-color' =>  $cor,
                                        'marker-size'   =>  'medium',
                                        'id'            =>  $value['id'],
                                        'idTipo'        =>  $value['idtipo'],
                                        'tipo'          =>  $value['tipo'],
                                        'dataOcorre'    =>  $value['dataOcorre'],
                                        'tipoEstado'    =>  $value['tipoEstado']
                                    ),
                );
        };   

        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);
        return json_encode($allfeatures, JSON_PRETTY_PRINT);
    }


    public function HeatMap($nomeCampus){

        $geometry = DB::table('ocorrencia_osm as o')
        ->join('campus', 'o.campus_ID', '=', 'campus.id')
        ->selectRaw('ST_AsGeoJSON(o.geom) as geometry')
        ->where('campus.localizacao',$nomeCampus)
        ->get()
        ->toJSON();

        $original_data = json_decode($geometry, true);
        $features = array();

        foreach($original_data as $key => $value) {
            $features[] = array(
                'type' => 'Feature',
                'crs'       => array('type' => 'name','properties' => array( 'name' => 'EPSG:4326')),  
                'geometry'  => json_decode($value['geometry']),
            );
        };   

        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);
        return json_encode($allfeatures, JSON_PRETTY_PRINT);
    }
}


/*
    EXEMPLO MAIS AVANÇADO

return View::make('pages.home')
            ->with('numeroOvelhas', $this->numeroOvelhas())

    		->with('numeroAlertasNaoVistos', $this->numeroAlertasNaoVistos())
            
            ->with('numeroColeiras', $this->numeroColeiras())
            ->with('numeroFarois', $this->numeroFarois())

*/