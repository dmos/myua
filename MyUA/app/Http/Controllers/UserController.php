<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use View;


class UserController extends Controller
{

    public function login(){
        return view('pages.utilizador.login');
    }

    public function checkLogin(){
        if($_REQUEST['username'] =="admin" && $_REQUEST['password'] == "admin"){
            //$_SESSION['username'] = "admin";
            return redirect("/mapas");
        }
        return redirect("/");
    }

    public function logout(){
        return redirect("/");
    }

    public function listaUtilizadores(){
        return view("pages.utilizador.listaUtilizadores");
    }

    public function listaUtilizadoresJSON(){
        
        $query=DB::table('utilizador as u')
        ->join('utilizador_tipo as ut', 'u.tipo_ID','ut.id')
        ->select('u.id','u.nome','ut.tipo as tipoUtilizador','u.email')
        ->get();
        
        
        //->get()
        //->toJson();
        //$query=json_decode($query,true);

        $res = array();
        foreach ($query as $ocorrencia) {
            $temp=array();
            $temp[]=$ocorrencia->id;
            $temp[]=$ocorrencia->nome;
            $temp[]=$ocorrencia->tipoUtilizador;
            $temp[]=$ocorrencia->email;
            $res[]=$temp;
        }


        $data=array(
            'data'=>$res
        );
        //dd($data);
        return json_encode($data);
    }

    public function utilizador($idUtilizador){
        //TODO : mais inforações
        $utilizador=DB::table('utilizador as u')
        ->join('utilizador_tipo as ut', 'u.tipo_ID','ut.id')
        ->select('u.id','u.nome','ut.tipo as tipoUtilizador','u.email')
        ->where('u.id',$idUtilizador)
        ->first();

        return view("pages.utilizador.utilizador")->with('utilizador',$utilizador);
    }
    
}
