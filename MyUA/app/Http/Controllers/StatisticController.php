<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use View;

class StatisticController extends Controller
{
    public function index(){
        return view('pages.statistics');
    }
    

    public function OcorrenciasEstado($campus){
        try{
            if($campus == "Geral"){
                $dados['dados'] = DB::table('ocorrencia_osm as o')
                ->join('estado as e','o.estado_ID','e.id')
                ->select(DB::raw("count(o.id) as total, e.tipo as label"))
                ->groupBy('e.tipo')
                ->get();
            }
            else{
                $dados['dados'] = DB::table('ocorrencia_osm as o')
                ->join('estado as e','o.estado_ID','e.id')
                ->join('campus', 'o.campus_ID', '=', 'campus.id')
                ->select(DB::raw("count(o.id) as total, e.tipo as label"))
                ->where('campus.localizacao',$campus)
                ->groupBy('e.tipo')
                ->get();    
            }
            
            
            if($dados['dados']->isEmpty()){
                $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
            }
        }
        catch (Exception $e) {
            $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
        }

    	return json_encode($dados);
    }
    public function OcorrenciasTipo($campus){
        try{
            if($campus == "Geral"){
                $dados['dados'] = DB::table('ocorrencia_osm as o')
                ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
                ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
                ->select(DB::raw("count(o.id) as total, t.tipo as label"))
                ->groupBy('t.tipo')
                ->get();
            }
            else{
                $dados['dados'] = DB::table('ocorrencia_osm as o')
                ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
                ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
                ->join('campus', 'o.campus_ID', '=', 'campus.id')
                ->select(DB::raw("count(o.id) as total, t.tipo as label"))
                ->where('campus.localizacao',$campus)
                ->groupBy('t.tipo')
                ->get();
            }

            if($dados['dados']->isEmpty()){
                $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
            }
        }
        catch (Exception $e) {
            $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
        }

    	return json_encode($dados);
    }
    
}
