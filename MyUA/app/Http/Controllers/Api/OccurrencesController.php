<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use PDO;

class OccurrencesController extends Controller
{
    public function index()
    {
        $query=DB::table('ocorrencia_osm as o')
        ->join('campus as c', 'o.campus_ID','c.id')
        ->join('estado as e', 'o.estado_ID', 'e.id')
        ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
        ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
        ->select('o.id','o.dataOcorre','o.dataResolv', 'o.descricao', 'o.imagem', 'e.tipo as estado', 'c.localizacao','t.tipo')
        ->get();

        $res = array();
        foreach ($query as $ocorrencia) {
            $temp=array();
            $temp[]=$ocorrencia->id;
            $temp[]=$ocorrencia->localizacao;
            $temp[]=$ocorrencia->tipo;
            $temp[]=$ocorrencia->estado;
            $temp[]=$ocorrencia->dataOcorre;
            $temp[]=$ocorrencia->dataResolv;
            $temp[]=$ocorrencia->descricao;
            $temp[]=$ocorrencia->imagem;
            $res[]=$temp;
        }


        $data=array(
            'data'=>$res
        );
        //dd($data);
        return json_encode($data);
    }

    public function getOccurrences($nomeCampus)
    {
        $query=DB::table('ocorrencia_osm as o')
        ->join('campus', 'o.campus_ID', '=', 'campus.id')
        ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
        ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
        ->selectRaw('ST_AsGeoJSON(o.geom) as geometry, o.id, o."dataOcorre", o."estado_ID", o."dataResolv", o."imagem"')
        ->where('campus.localizacao',$nomeCampus)
        ->get()
        ->toJSON();
        $original_data = json_decode($query, true);
        $features = array();

        foreach($original_data as $key => $value) {
            if($value['estado_ID'] == 1){
                $cor = "#cc522c"; //aberto
            }
            elseif ($value['estado_ID'] == 2) {
                $cor = "#efff7a"; //em resolução
            }
            else{
                $cor = "#42f459";//resolvido
            }
            //dd($value['geometry']);
            $features[] = array(
                'type' => 'Feature',
                'crs'       => array('type' => 'name','properties' => array( 'name' => 'EPSG:4326')),
                'geometry' => json_decode($value['geometry']),
                'properties'    => array(
                                        'id' => $value['id'],
                                        'marker-color'=> $cor,
                                        'marker-size'=> 'medium',
                                        'name'  => 'State Park'
                                    ),
                );
                $cenas[] = array('geometry' => json_decode($value['geometry']));
        };

        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        return $cenas;
    }

    public function addReport(Request $request) {
        $data = $request;
        $toSend = [];
        $toSend[0] = 'ST_SetSRID(ST_MakePoint($data->geom[0], $data->geom[1]), 4326)';
        $toSend[1] = date('Y-m-d');
        $toSend[2] = null;

        switch ($data->state){
            case 'open':
                $toSend[3] = 1;
                break;
            case 'ongoing':
                $toSend[3] = 2;
                break;
            case 'closed':
                $toSend[3] = 3;
                break;
        }

        $toSend[4] = 1;
        $toSend[5] = 1;

        switch ($data->campus){
            case 'estga':
                $toSend[6] = 3;
                break;
            case 'ua':
                $toSend[6] = 1;
                break;
            case 'esan':
                $toSend[6] = 4;
                break;
        }
        $toSend[7] = $data->image === null ? null : $data->image;
        $toSend[8] = $data->description;
        $lat  = $data->geom[0];
        $long = $data->geom[1];
        try {

            $user = 'pedro';
            $dbh = new PDO('pgsql:host=localhost;dbname=sig', $user);

            $dbh->beginTransaction();
            //$this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            $stmt = $dbh->prepare('INSERT INTO ocorrencia_osm(geom, "dataOcorre", "dataResolv", "estado_ID", "utilizador_ID",
            "operador_ID", "campus_ID", imagem, descricao) VALUES (ST_Transform(ST_SetSRID(ST_MakePoint(?, ?), 3857), 4326),
            ?, ?, ?, ?, ?, ?, ?, ?);');

            if ($stmt->execute(array($lat, $long, $toSend[1], null, $toSend[3], $toSend[4], $toSend[5],
             $toSend[6], $toSend[7], $toSend[8]))) {
                print_r("OK");
            } else {
                print_r($stmt->errorInfo());
            }
            $lastid = $dbh->lastInsertId();
            $dbh->commit();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            $dbh->rollBack();
            die();
        }
        switch($data->type) {
            case 'trees and green spaces':
                $type = 5;
                break;
            case 'security and noises':
                $type = 6;
                break;
            case 'roads and sinalization':
                $type = 4;
                break;
            case 'garbage':
                $type = 2;
                break;
            case 'sewers':
                $type = 7;
                break;
            case 'exterior illumination':
                $type = 3;
                break;
            case 'sidewalks/accessability':
                $type = 1;
                break;
        }

        try{
            $user = 'pedro';
            $dbh = new PDO('pgsql:host=localhost;dbname=sig', $user);

            $dbh->beginTransaction();
            //$this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            $stmt = $dbh->prepare('INSERT INTO ocorrencia_rel_tipo("osm_ID", "tipo_ID") VALUES (?, ?)');

            if ($stmt->execute(array($lastid, $type))) {
                print_r("OK");
            } else {
                print_r($stmt->errorInfo());
            }
            $dbh->commit();
        }catch(PDOException $e){
            print "Error!: " . $e->getMessage() . "<br/>";
            $dbh->rollBack();
            die();
        }
        //ST_SetSRID(ST_MakePoint($data->geom[0], $data->geom[1]), 4326);

        return null;
    }


}