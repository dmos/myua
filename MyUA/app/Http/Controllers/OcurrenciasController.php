<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use View;

class OcurrenciasController extends Controller
{
    public function index(){
        return view("pages.ocorrencia.listaOcurrencias");
    }

    public function listaOcurrenciasJSON(){
        
        $query=DB::table('ocorrencia_osm as o')
        ->join('campus as c', 'o.campus_ID','c.id')
        ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
        ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
        ->select('o.id','o.dataOcorre','o.dataResolv','c.localizacao','t.tipo')
        ->get();
        
        
        //->get()
        //->toJson();
        //$query=json_decode($query,true);

        $res = array();
        foreach ($query as $ocorrencia) {
            $temp=array();
            $temp[]=$ocorrencia->id;
            $temp[]=$ocorrencia->localizacao;
            $temp[]=$ocorrencia->tipo;
            $temp[]=$ocorrencia->dataOcorre;
            $temp[]=$ocorrencia->dataResolv;
            $res[]=$temp;
        }


        $data=array(
            'data'=>$res
        );
        //dd($data);
        return json_encode($data);
    }


    public function ocurrencia($idOcurrencia){
        $ocorrencia=DB::table('ocorrencia_osm as o')
        ->join('campus as c', 'o.campus_ID','c.id')
        ->join('estado as e', 'o.estado_ID','e.id')
        ->leftJoin('operador as op', 'o.operador_ID','op.id')
        ->join('ocorrencia_rel_tipo as o_t','o.id','o_t.osm_ID')
        ->join('ocorrencia_tipo as t','o_t.tipo_ID','t.id')
        ->select('o.id as nOcorrencia','o.dataOcorre as dataOcorrencia','o.dataResolv as dataResolvida','o.imagem','o.descricao','c.localizacao','t.tipo','e.tipo as estado','op.nome as nomeOperador')
        ->where('o.id',$idOcurrencia)
        ->first();

        $estados=DB::table('estado')
        ->select("id","tipo as estado")
        ->get();
        $operadores=DB::table('operador')
        ->select("id","nome as operador")
        ->orderBy('id')
        ->get();
        
        return view("pages.ocorrencia.ocurrencia")->with('ocorrencia',$ocorrencia)
        ->with('estados',$estados)
        ->with('operadores',$operadores);
    }

    public function ocurrenciaGeoJSON($idOcurrencia){


        $geometry = DB::table('ocorrencia_osm')
        ->selectRaw('ST_AsGeoJSON(geom) as geometry, ocorrencia_osm."estado_ID"')
        ->where('ocorrencia_osm.id',$idOcurrencia)
        ->get()
        ->toJSON();

        $original_data = json_decode($geometry, true);
        $features = array();

        foreach($original_data as $key => $value) {
            if($value['estado_ID'] == 1){
                $cor = "#cc522c"; //aberto
            }
            elseif ($value['estado_ID'] == 2) {
                $cor = "#efff7a"; //em resolução
            }
            else{
                $cor = "#42f459";//resolvido
            }
            //dd($value['geometry']);
            $features[] = array(
                'type' => 'Feature',
                'crs'       => array('type' => 'name','properties' => array( 'name' => 'EPSG:4326')),  
                'geometry' => json_decode($value['geometry']),
                'properties'    => array('marker-color'=> $cor,
                                        'marker-size'=> 'medium',
                                        'name'  => 'State Park'
                                    ),
                );
        };   

        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);
        return json_encode($allfeatures, JSON_PRETTY_PRINT);


    
        //https://openlayers.org/en/latest/examples/geojson.html
        //https://gist.github.com/wboykinm/5730504

        //https://astuntechnology.github.io/osgis-ol3-leaflet/ol3/02-GEOJSON.html
        //https://stackoverflow.com/questions/24275119/reformatting-json-to-geojson-in-php-laravel?answertab=active#tab-top
    }

    public function update($idOcurrencia){
        //dd($_POST);
        $idOperador = $_POST['operador'];
        if($idOperador == 0) $idOperador = NULL;
        $temDataResolvido=DB::table('ocorrencia_osm')
            ->select('dataResolv')
            ->where('id',$idOcurrencia)
            ->first();

        if($_POST['estado'] == 3  && $temDataResolvido->dataResolv == null){
            $dataResolvido = date('Y-m-d');
            DB::table('ocorrencia_osm')
            ->where('id', $idOcurrencia)
            ->update(['estado_ID' => $_POST['estado'],'operador_ID'=>$idOperador,'dataResolv' => $dataResolvido]);
        }
        else{
            DB::table('ocorrencia_osm')
            ->where('id', $idOcurrencia)
            ->update(['estado_ID' => $_POST['estado'],'operador_ID'=>$idOperador,'dataResolv' => null]);
        }

        return redirect('ocurrencia/'.$idOcurrencia);
    }

}
