<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*  default
Route::get('/', function () {
    return view('welcome');
});
*/

/*--------------    Dashboard   -----------------*/
Route::get('/mapas',['as'=>'mapas','uses'=>'MapsController@index']);
Route::get('/OcorrenciasCampus/{nomeCampus}',['as'=>'OcorrenciasCampus','uses'=>'MapsController@OcorrenciasCampus']);
Route::get('/HeatMap/{nomeCampus}',['as'=>'HeatMap','uses'=>'MapsController@HeatMap']);

/*--------------    Ocorrencias   -----------------*/
Route::get('/listaOcurrencias',['as'=>'Lista de ocorrencias','uses'=>'OcurrenciasController@index']);
Route::get('/listaOcurrenciasJSON',['as'=>'Lista de ocorrencias','uses'=>'OcurrenciasController@listaOcurrenciasJSON']);

Route::get('/ocurrencia/{idOcurrencia}',['as'=>'Ocorrencia','uses'=>'OcurrenciasController@ocurrencia']);
Route::get('/ocurrenciaGeoJSON/{idOcurrencia}',['as'=>'Ocorrencia','uses'=>'OcurrenciasController@ocurrenciaGeoJSON']);
Route::post('/ocurrencia/{idOcurrencia}/update',['as'=>'Ocorrencia','uses'=>'OcurrenciasController@update']);




/*--------------    Utilizadores   -----------------*/
Route::get('/',['as'=>'Login','uses'=>'UserController@login']);
Route::post('/checkLogin',['as'=>'Login','uses'=>'UserController@checkLogin']);
Route::get('/logout',['as'=>'logout','uses'=>'UserController@logout']);


Route::get('/listaUtilizadores',['as'=>'Lista de utilizadores','uses'=>'UserController@listaUtilizadores']);
Route::get('/listaUtilizadoresJSON',['as'=>'Lista de utilizadores','uses'=>'UserController@listaUtilizadoresJSON']);
Route::get('/utilizador/{idUtilizador}',['as'=>'Utilizador','uses'=>'UserController@utilizador']);


/*--------------    Statistics   -----------------*/
Route::get('/estatisticas',['as'=>'Estatísticas','uses'=>'StatisticController@index']);
Route::get('/OcorrenciasEstado/{campus}',['as'=>'get Estatísticas','uses'=>'StatisticController@OcorrenciasEstado']);
Route::get('/OcorrenciasTipo/{campus}',['as'=>'get Estatísticas','uses'=>'StatisticController@OcorrenciasTipo']);
