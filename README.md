# MyUA

MyUA is a backend **web application** developed to manage incoming reported problems in **university campus** by thier students. This project was a proof of concept to the university that using **GIS** and **WEB** tecnology together with the **community**, there is a way to make the campus better.

This project was developed for a class for the **University of Aveiro**.

![Dashboard](./images/dashboard.PNG)



## Core Tecnologies

* HTML
* CSS
* Bootstrap
* JavaScript
* JQuery
* PHP
* JSON
* AJAX
* PostgreSQL
* GIS
* ChartJS
* OpenLayers



## Main Functionality

* [x] Dashboard with Map
* [x] CRUD Users
* [x] CRUD Occurrences
* [x] Prefrences
* [x] Statistics


## Getting Started

1. `git clone ...`
2. copy project folder to web server
3.  `cd` into folder
4. Run `composer install` to install dependencies
5. Run `php artisan key:generate` to generate unique application key
6. Change `.env` file with database credecials
7. restore **db.pgsql** to postgresql
8. Copy project folder to web server
9. Enjoy

___

## Other fotos
### heatmap
![Heatmap](./images/heatmap.PNG)

### Lists 
![List](./images/lista.PNG)

### Occurrences 
![Occurrences](./images/occur.PNG)

### Stats 
![Stats](./images/stats.PNG)

___

## Developed By
* Diogo Santos
* Jorge Godinho
* Pedro Quinta